package ru.intabia.krasnov.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/secure")
public class SecureController {

    @GetMapping
    public String getTestEndpoint(){
        return "This is secure information";
    }
}
